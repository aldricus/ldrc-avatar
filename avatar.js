LdrcAvatar = {
	errorMessages: function(err) {},
	successMessages: function() {},
}

var createSquare = function(fileObj, readStream, writeStream) {
	//todo change name, md5...: fileObj.name('file_name')
	gm(readStream, fileObj.name()).resize('400', '400').stream().pipe(writeStream);
};

avatarPicStore = new FS.Store.GridFS("ldrcAvatars", {
	transformWrite: createSquare,
	maxTries: 2, // optional, default 5
	chunkSize: 1024 * 1024
});


LdrcAvatars = new FS.Collection("ldrcAvatars", {
	stores: [ /*avatarStore*/ avatarPicStore]
});