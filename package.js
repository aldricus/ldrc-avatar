Package.describe({
  name: 'aldricus:avatar',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: 'Avatar management',
  // URL to the Git repository containing the source code for this package.
  git: 'https://bitbucket.org/aldricus/ldrc-avatar',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.0.3.2');
  api.use('templating'); // Required to load template
  api.use('cfs:standard-packages');
  api.use('cfs:gridfs');
  /*api.use('cfs:filesystem');
  api.use('cfs:s3');*/
  api.use('cfs:graphicsmagick');
  api.use('accounts-ui');
  api.addFiles([
    'client/avatar.css',
    'client/ldrc-avatar.html',
    'client/ldrc-avatar-edit.html',
    'client/client-avatar.js',
  ], [
    'client'
  ]);
  api.addFiles([
    'avatar.js'
  ], [
    'client',
    'server'
  ]);
  api.addAssets([
    'spinner-bluey.gif',
    'anonymous.png',
    'check50.png',
    'check50-green.png'
  ], ['client']);
  api.addFiles(['server/avatar-gridfs.js'], ['server']);
  api.export('LdrcAvatar');

});


Package.onTest(function(api) {
  api.imply('practicalmeteor:sinon');
  api.use('tinytest', ['client', 'server']);

  api.imply('underscore', ['client', 'server']);
  api.imply('jquery', ['client']);

  api.use('templating', 'client');
  api.use('aldricus:avatar', ['client', 'server']);
  api.use('cfs:standard-packages');
  api.use('cfs:gridfs');
  /*api.use('cfs:filesystem');
  api.use('cfs:s3');*/
  api.use('cfs:graphicsmagick');
  api.use('accounts-ui');
  api.use('blaze-html-templates', 'client');
  api.addFiles('tests/avatar-test-stubs.js', 'client');
  api.addFiles('tests/avatar-tests.js', 'client');
});