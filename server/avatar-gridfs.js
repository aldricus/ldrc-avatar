Meteor.LdrcAvatar = function() {

}
Meteor.LdrcAvatar.prototype.init = function() {
    var filter = {
        maxSize: 1048576, // in bytes
        allow: {
            contentTypes: ['image/*'],
            extensions: ['png', 'jpeg', 'jpg']
        },
        deny: {
            /*contentTypes: ['image/*'],
              extensions: ['png', 'jpeg', 'jpg']*/
        }
    };


    LdrcAvatars.filters(filter);

    LdrcAvatars.deny({
        insert: function(userId, fileObj) {
            if (fileObj.owner === null || userId === null) {
                console.log('Deny Avatar insert: ' + userId);
                return true;
            } else {
                console.log('Avatar insert Allowed: ' + (userId === fileObj.owner));
                return !(userId === fileObj.owner);
            }
        },
        update: function(userId, fileObj) {
            if (fileObj.owner === null || userId === null) {
                console.log('Deny Avatar update: ' + userId);
                return true;
            } else {
                console.log('Avatar update Allowed: ' + (userId === fileObj.owner));
                return !(userId === fileObj.owner);
            }
        },
        remove: function() {
            return true;
        }
    });


    LdrcAvatars.allow({
        insert: function(userId, fileObj) {
            return true;
        },
        update: function(userId, fileObj) {
            return true;
        },
        remove: function(userId, doc) {
            return false;
        },
        download: function(userId, doc) {
            // Required so file can be downloaded, "Access denied" otherwise
            return true;
        }
    });

    // Publish custom field: avatar
    Meteor.publish('ldrcUserData', function(userid) {
        check(userid, String);
        return Meteor.users.find({
            _id: userid
        }, {
            fields: {
                avatar: 1,
                username: 1,
            }
        });
    });

    Meteor.publish("ldrcAvatars", function() {
        return LdrcAvatars.find();
    });
}


var LdrcAvatar = new Meteor.LdrcAvatar();
LdrcAvatar.init( /*TODO:options*/ );