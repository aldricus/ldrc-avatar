/*
    Meteor Simple Avatar Management - Copyright (C) 2015  Aldric T
    aldric.dev@gmail.com

    License:  GNU General Public License

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
Meteor.startup(function() {});

var avatarHandle;
var avatarInputChanged = false;
var avatarUserHandle;
var errors = [];
var tempFiles = [];

Template.ldrcAvatar.helpers({
	avatarLoaded: function() {
		return avatarHandle && avatarHandle.ready() &&
			avatarUserHandle && avatarUserHandle.ready();
	},
	size: function() {
		if (isNaN(this.size) && _.contains(['small', 'medium', 'large'], this.size)) {
			return 'ldrc-avatar-' + this.size;
		}
	},
	style: function() {
		if (!isNaN(this.size)) {
			return {
				style: 'width:' + this.size + 'px; height:' + this.size + 'px'
			};
		}
	},
	shape: function() {
		if (this.shape !== undefined && _.contains(['rounded', 'square'], this.shape)) {
			return 'ldrc-avatar-' + this.shape;
		} else {
			return 'ldrc-avatar-rounded';
		}
	},
	src: function() {
		var src = '';
		if (avatarHandle && avatarHandle.ready() &&
			avatarUserHandle && avatarUserHandle.ready()) {

			var user = Meteor.users.find({
				_id: this.userid
			}).fetch()[0];
			if (user && user.avatar !== undefined) {
				var fsFile = LdrcAvatars.find({
					_id: user.avatar
				}).fetch()[0];
				src = fsFile.url();
			} else {
				src = '/packages/aldricus_avatar/anonymous.png';
			}
			Template.instance().$('.avatar-edit-label').css('visibility', 'visible');

			return {
				src: src
			};
		}
	},
	username: function() {
		if (avatarUserHandle && avatarUserHandle.ready()) {
			var user = Meteor.users.find({
				_id: this.userid
			}).fetch()[0];
			return this.username == true ? user.username : '';
		}
		//TODO reactive !!!!!
	}
});


Template.ldrcAvatar.created = function() {
	// Subscription handle so we know when data are available
	if (Template.instance().data.userid) {
		avatarUserHandle = Meteor.subscribe('ldrcUserData', Template.instance().data.userid);
	}
	avatarHandle = Meteor.subscribe('ldrcAvatars');
};

Template.ldrcAvatarEdit.events({
	'change #avatarupload': function(event, template) {
		errors = [];
		avatarInputChanged = true;
		$('.ldrc-avatar-edit .errors').css('display', 'none');
		var files = $('#avatarupload')[0].files;
		if (files.length === 0) {
			files = tempFiles;
		} else {
			tempFiles = files;
		}
		_.each(files, function(file) {
			var fsFile = new FS.File(file);
			// Client size validation
			// Must be validated in server side
			// because the client can be
			// easily modified
			// TODO: what if not supported by browser?
			if (!_.contains(['image/jpeg', 'image/png', 'image/jpg'], fsFile.type())) {
				errors.push('Format not allowed: ' + fsFile.type() + '. Must be JPEG or PNG');
			}
			if (1024 * 1024 < fsFile.size()) {
				errors.push('Max size exceeded: 1Mb');
				template.$('.ldrc-avatar-edit span').html('Error').css('color', 'red');
			}
			if (errors.length === 0) {
				var $preview = template.$('.ldrc-avatar > img');
				if (window.FileReader && window.File && window.FileList && window.Blob) {
					var reader = new FileReader();
					reader.onloadend = function() {
						$preview.attr('src', reader.result);
					}
					if (file) {
						reader.readAsDataURL(file);
						template.$('.ldrc-avatar-edit span.avatar-edit-label').html('Non charger').css('color', 'red');
					} else {
						$preview.attr('src', '');
					}
				} else {
					//TODO: ajax
					alert('fileReader not supported');
				}
			} else {
				var html = '';
				_.each(errors, function(error) {
					html += '<p><span>' + error + '</span></p>';
				});
				template.$('.ldrc-avatar-edit .errors').html(html);
				template.$('.ldrc-avatar-edit .errors').css('display', 'block');
			}
		});
	},
	'click .ldrc-avatar-edit span': function(event, template) {
		event.preventDefault();
		template.$('#avatarupload').trigger('click');
	},
	'click #ldrcSendAvatar': function(event, template) {
		event.preventDefault();
		var files = tempFiles;

		if (files.length > 0 && avatarInputChanged && errors.length === 0) {
			template.$('.avatar-edit-label').css('visibility', 'hidden');
			var userId = Meteor.userId();
			_.each(files, function(file) {
				template.$('load-image-edit').css('display', 'block');
				//TODO: Update OR Delete previous avatar?
				var fsFile = new FS.File(file);
				fsFile.owner = userId;

				var callback = function(err, fileObj) {
					if (err) {
						console.error(err);
						LdrcAvatar.errorMessages(err);
					} else {
						Meteor.users.update(userId, {
							$set: {
								"avatar": fileObj._id
							}
						}, function(err) {
							template.$('load-image-edit').css('display', 'block');
							if (err) {
								console.error(err);
								LdrcAvatar.errorMessages(err);
								template.$('.ldrc-avatar-edit span').html('Réessayer').css('color', 'red');
								return;
							}
							if (LdrcAvatar.sucessMessages) {
								LdrcAvatar.sucessMessages();
								template.$('.ldrc-avatar-edit span').html('Modifier').css('color', 'green');
							}
						});
					}
					template.$('.avatar-edit-label').css('visibility', 'visible');
					// prevent from submitting several times
					avatarInputChanged = false;
				};
				LdrcAvatars.insert(fsFile, callback);
			});
		}
	}
});

Template.ldrcAvatarEdit.onRendered(function() {
	Meteor.subscribe('ldrcAvatars');
});
Template.ldrcAvatarEdit.helpers({
	avatars: function() {
		return LdrcAvatars.find(); // Where LdrcAvatars is an FS.Collection instance
	},
	size: function() {
		if (isNaN(this.size) && _.contains(['small', 'medium', 'large'], this.size)) {
			return 'ldrc-avatar-' + this.size;
		}
	},
	currentUserId: function() {
		if (!Template.instance().data.userid && Meteor.userId()) {
			avatarUserHandle = Meteor.subscribe('ldrcUserData', Meteor.userId());
		}
		return Meteor.userId();
	}
});
var src = '/packages/aldricus_avatar/';
Template.ldrcCheckBtn.events({
	'mouseover, touchstart': function(event, template) {
		event.preventDefault();
		template.$('input').attr('src', src +
			'check50-green.png');
	},
	'mouseout, touchend': function(event, template) {
		event.preventDefault();
		template.$('input').attr('src', src + 'check50.png');
	}
});